# Wikimedia Design Landing Page

View the page at https://design.wikimedia.org.
Imported from https://gerrit.wikimedia.org/r/plugins/gitiles/design/landing-page/.


## Blubber container image

Blubber images for design-landing-page page.

### Local development

Build image locally:
```
DOCKER_BUILDKIT=1 docker build --target production -f .pipeline/blubber.yaml .
```

Run image:
```
docker run -p 8080:8080 <image name>
```

Visit `http://127.0.0.1:8080/`

### Publish new image version

To create a new image version merge your change into the master branch.

This triggers the publish-image pipeline. Image is available at `docker-registry.wikimedia.org/repos/sre/miscweb/design-landing-page:<timestamp>`

## Deploy changes

- Merge changes to master branch.

- Update image version on [deployment-charts](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/deployment-charts/%2B/refs/heads/master/helmfile.d/services/miscweb/values-design-landing-page.yaml#3) with [latest value](https://gitlab.wikimedia.org/repos/sre/miscweb/design-landing-page/-/jobs/). _The value is in the `#13` step in the job 'publish-image'._

- Add wmf-sre-collab (group) as reviewer and wait for merge.

- Deploy to production from the deployment server. 

See [WikiTech: Miscweb](https://wikitech.wikimedia.org/wiki/Miscweb#Deploy_to_Kubernets).